import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-curso', /* app-input-property */
  templateUrl: './input-property.component.html',
  styleUrls: ['./input-property.component.css']
  /* inputs segue o mesmo esquema do style e pode ser colocado assim:
  inputs: [nomeCurso:nome] | nomeVar:NomeVarExternamente */
})
export class InputPropertyComponent implements OnInit {

  /**
   * o que está dentro da chaves é uma maneira de referenciar o input externamente
   * mas o correto é que ele seja usado com o próprio nome da variável
   */
  @Input('nome') nomeCurso: string = '';

  constructor() { }

  ngOnInit() { }

}
