import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DropdownService } from '../shared/services/dropdown.service';
import { EstadoBr } from '../shared/models/estado-br';
import { ConsultaCepService } from '../shared/services/consulta-cep.service';
import { Observable, empty } from 'rxjs';
import { FormValidations } from '../shared/form-validations';
import { VerificaEmailService } from './services/verifica-email.service';
import { map, tap, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { BaseFormComponent } from '../shared/base-form/base-form.component';

@Component({
	selector: 'app-data-form',
	templateUrl: './data-form.component.html',
	styleUrls: ['./data-form.component.css']
})
export class DataFormComponent extends BaseFormComponent implements OnInit {

	// -> formulario: FormGroup
	estados: EstadoBr[];
	// estados: Observable<EstadoBr[]>;
	cargos: any[];
	tecnologias: any[];
	newsletterOp: any[];
	cidades = [];
	frameworks = ['Angular', 'React', 'Vue', 'Sencha'];

	private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
	
	constructor(
		private formBuilder: FormBuilder,
		private http: HttpClient,
		private dropdownService: DropdownService,
		private cepService: ConsultaCepService,
		private verificaEmailService: VerificaEmailService
	) {
		super();
	 }

	ngOnInit() {

		this.verificaEmailService.verificarEmail('email@email.com').subscribe();

		this.montarFormulario();
		// this.dropdownService.getEstadoBr().subscribe(dados => this.estados = dados);
		this.dropdownService.getEstadoBr().subscribe(dados => this.estados = dados);
		this.cargos = this.dropdownService.getCargos();
		this.tecnologias = this.dropdownService.getTecnologias();
		this.newsletterOp = this.dropdownService.getNewsLetter();

		this.formulario.get('endereco.cep').statusChanges
			.pipe(
				distinctUntilChanged(),
				tap(value => console.log(`Valor CEP: ${value}`)),
				switchMap(status => status == 'VALID' ? 
					this.cepService.consultarCep(this.formulario.get('endereco.cep').value)
					: empty())
			)
			.subscribe(dados => dados ? this.popularDadosForm(dados) : {});
			
			this.formulario.get('endereco.estado').valueChanges
			.pipe(
				tap(estado => console.log('Novo estado', estado)),
				map(estado => this.estados.filter(e => e.sigla === estado)),
				map(estados => estados && estados.length > 0 ? estados[0].id : empty()),
				switchMap((estadoId: number) => this.dropdownService.getCidades(estadoId)),
				tap(console.log)
			)
			.subscribe(cidades => this.cidades = cidades);
			// this.dropdownService.getCidades(8).subscribe(console.log);
	}

	montarFormulario() {
		this.formulario = this.formBuilder.group({
			nome: [null, [Validators.required, Validators.minLength(3)]],
			email: [null, [Validators.required, Validators.email], this.verificarEmail.bind(this)],
			confirmarEmail: [null, [FormValidations.equalsTo('email')]],
			endereco: this.formBuilder.group({
				cep: [null, [Validators.required, FormValidations.cepValidator]],
				numero: [null],
				complemento: [null, Validators.required],
				rua: [null, Validators.required],
				bairro: [null, Validators.required],
				cidade: [null, Validators.required],
				estado: [null, Validators.required]
			}),
			cargo: [null],
			tecnologias: [null],
			newsletter: ['s'],
			termos: [null, Validators.pattern('true')],
			frameworks: this.buidFrameworks()
		});
	}

	buidFrameworks() {
		const values = this.frameworks.map(v => new FormControl(false))
		
		return this.formBuilder.array(values, FormValidations.requiredMinCheckbox(2));
		// return [new FormControl(false), new FormControl(false), new FormControl(false), ... ]
	}

	submit() {
		let valueSubmit = Object.assign({}, this.formulario.value);

		valueSubmit = Object.assign(valueSubmit, {
			frameworks: valueSubmit.frameworks.map((valor, i) => valor ? this.frameworks[i] : null)
		});

		console.log(valueSubmit);
		this.http.post('https://httpbin.org/post', JSON.stringify(this.formulario.value), this.options)
			.subscribe(dados => this.formulario.reset(), error => alert('Erro'));
	}

	consultarCep() {
		let cep = this.formulario.get('endereco.cep').value;
		if(cep != null && cep !== '') {
			this.cepService.consultarCep(cep)
			.subscribe(dados => this.popularDadosForm(dados));
		}
	}

	popularDadosForm(dados) {
		// setValue do template drive também funciona
		// com a mesma condição, só que ele reseta todos os campos;
		this.formulario.patchValue({
			endereco: {
				rua: dados.logradouro,
				complemento: dados.complemento,
				bairro: dados.bairro,
				cidade: dados.localidade,
				estado: dados.uf
			}
		})
		// Popular apenas um campo
		// this.formulario.get('nome').setValue('Willian')
	}

	resetarDadosForm() {
		this.formulario.patchValue({
			endereco: {
				rua: '',
				complemento: '',
				bairro: '',
				cidade: '',
				estado: ''
			}
		})
	}


	setarCargo() {
		const cargo = { nome: 'Dev', nivel: 'Pleno', desc: 'Dev Pl' };
		this.formulario.get('cargo').setValue(cargo);
	}

	compararCargos(obj1, obj2) {
		return obj1 && obj2 ? (obj1.nome === obj2.nome && obj1.nivel === obj2.nivel) : obj1 === obj2;
	}

	setarTecnologia() {
		this.formulario.get('tecnologias').setValue(['java', 'php']);
	}

	verificarEmail(formControl: FormControl) {
		return this.verificaEmailService.verificarEmail(formControl.value)
			.pipe(map(emailExiste => emailExiste ? { emailInvalido : true } : null));
	}
}
