import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

// Essas 3 config foram pra funcionar o 'pt-BR'
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
registerLocaleData(localePt);

import { AppComponent } from './app.component';
import { ExemplosPipesComponent } from './exemplos-pipes/exemplos-pipes.component';
import { CamelCasePipe } from './camel-case.pipe';
import { SettingsService } from './settings.service';
import { FiltroArrayPipe } from './filtro-array.pipe';
import { FormsModule } from '@angular/forms';
import { FiltroArrayImpuroPipe } from './filtro-array-impuro.pipe';

@NgModule({
	declarations: [
		AppComponent,
		ExemplosPipesComponent,
		CamelCasePipe,
		FiltroArrayPipe,
		FiltroArrayImpuroPipe,
	],
	imports: [
		BrowserModule,
		FormsModule
	],
	providers: [
		{
			// Seta os pipes para linguagem adicionada
			// Datas, valores e etc automaticamente ficam no padrão BR
			provide: LOCALE_ID,
			useValue: 'pt-BR' 
		}
		// SettingsService,
		// {
		// 	provide: LOCALE_ID,
		// 	deps: [SettingsService],
		// 	useFactory: (seetingsService) => seetingsService.getLocale()
		// }	
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
