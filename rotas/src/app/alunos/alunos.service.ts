import { Injectable } from '@angular/core';
import { Aluno } from './aluno';

@Injectable({
    providedIn: 'root'
})
export class AlunosService {

	// private alunos: any[] = []
	
	// Refactory
	private alunos: Aluno[] = [
		{id: 1, nome: 'Willian Ferreira', email: 'willian@gmail.com'},
		{id: 2, nome: 'Victor Ananias', email: 'victor@gmail.com'},
		{id: 3, nome: 'Ruan Vinicius', email: 'ruan@gmail.com'}
	]

	constructor() { }
	
	getAlunos() {
		return this.alunos;
	}

	getAluno(id: number): any {
		let alunos = this.alunos;
		for (let aluno of alunos) {
			if(aluno.id == id) {
				return aluno;
			}
		}
		return null;
	}
}
