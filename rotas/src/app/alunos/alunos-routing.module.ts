import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlunosComponent } from './alunos.component';
import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';

import { AlunosDeactivatedGuard } from '../guards/alunos-deactivated.guard';
import { ResolverGuard } from './guards/resolver.guard';

const routes: Routes = [
    // rota c/ id vem depois. O angular avalia as rotas sem parametros primeiro, então
    // por depois, ele pode encarar o 'novo' como o parametro e assim ir para o
    // component errado
    {path: '', component: AlunosComponent, children: [
        {path: 'novo', component: AlunoFormComponent},
        {
            path: ':id', 
            component: AlunoDetalheComponent,
            resolve: { aluno: ResolverGuard }
        },
        {
            path: ':id/editar', 
            component: AlunoFormComponent,
            canDeactivate: [AlunosDeactivatedGuard]
        },
    ]},

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AlunosRoutingModule { }
