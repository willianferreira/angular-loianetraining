import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { Aluno } from '../aluno';
import { AlunosService } from '../alunos.service';

@Injectable({
  	providedIn: 'root'
})
export class ResolverGuard implements Resolve<Aluno> {
	
	constructor(private service: AlunosService) { }
	
	resolve(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<any> | Promise<any> {

		let id = next.params['id'];

		return this.service.getAluno(id);
	}
}
