import { Directive, HostListener, HostBinding, ElementRef, Renderer, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  @HostListener('mouseenter') passarMouse(){
    this.backgroundColor = this.hightlightColor;
  }

  @HostListener('mouseleave') tirarMouse(){
    this.backgroundColor = this.corPadrao;
  }

  @HostBinding('style.backgroundColor') backgroundColor: String;

  @Input() corPadrao: String = 'blue';
  @Input() hightlightColor: String = 'yellow';

  constructor() { }

  ngOnInit() {
    this.backgroundColor = this.corPadrao;
  }

}
